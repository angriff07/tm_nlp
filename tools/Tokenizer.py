# -*- coding: utf-8 -*-
"""
Created on Fri Dec  4 13:31:04 2015

@author: aromanenko
"""

import re


class Tokenizer(object):
    _find_next_token_func = None
    _regex_ru = re.compile(ur'(?:[^a-zа-яё\s]+|[a-zа-яё]+)', re.IGNORECASE | re.UNICODE)
    _regex_en = re.compile(ur'(?:[^a-z\s]+|[a-z]+)', re.IGNORECASE | re.UNICODE)

    def __init__(self, lang=u"ru"):
        if lang == u"ru":
            self._find_next_token_func = self.__find_next_token_ru_default
        elif lang == u"en":
            self._find_next_token_func = self.__find_next_token_en_default
        else:
            print(u"Unknown language is specified. Please, specify find_next_token function")

    def __find_next_token_ru_default(self, line, start_ind):
        m = self._regex_ru.search(line, start_ind)
        if m:
            start_ind, length = m.start(), len(m.group())
        else:
            start_ind, length = start_ind, 0
        return start_ind, length

    def __find_next_token_en_default(self, line, start_ind):
        m = self._regex_en.search(line, start_ind)
        if m:
            start_ind, length = m.start(), len(m.group())
        else:
            start_ind, length = start_ind, 0
        return start_ind, length

    @property
    def regex_ru(self):
        return self._regex_ru

    @property
    def regex_en(self):
        return self._regex_en

    @regex_ru.setter
    def regex_ru(self, regex_string):
        self._regex_ru = re.compile(regex_string, re.IGNORECASE | re.UNICODE)

    @regex_en.setter
    def regex_en(self, regex_string):
        self._regex_en = re.compile(regex_string, re.IGNORECASE | re.UNICODE)

    @property
    def find_next_token_func(self):
        return self._find_next_token_func
        
    @find_next_token_func.setter
    def find_next_token_func(self, func):
        self._find_next_token_func = func

    def find_indexes(self, list_of_lines):
        index_list = []
        for line in list_of_lines:
            index_list += [[]]
            start_ind, length = 0, 0
            while True:
                start_ind, length = self.find_next_token_func(line, start_ind+length)
                if length == 0:
                    break
                index_list[-1] += (start_ind, length)
        return index_list

    def tokenize(self, list_of_lines, delimiter=u' '):
        index_list = self.find_indexes(list_of_lines)
        tokenized = []
        for line,index_line in zip(list_of_lines, index_list):
            temp_list = []
            for ind,length in zip(index_line[0::2], index_line[1::2]):
                temp_list += [line[ind:ind+length]]
            if delimiter is None:
                tokenized += [temp_list]
            else:
                tokenized += [delimiter.join(temp_list)]
        return tokenized, index_list


if __name__ == "__main__":
    print(u"Russian version (default)")
    t_ru = Tokenizer()
    line = u"   Мама  мыла раму, или Daddy what are you doing? 123"
    texts, indexes = t_ru.tokenize([line])
    print(line)
    print(texts[0])
    print(indexes[0])

    print(u"English version")
    t_en = Tokenizer(lang=u"en")
    line = u"Мама мыла раму, или Daddy what are you doing? 123"
    texts, indexes = t_en.tokenize([line])
    print(line)
    print(texts[0])
    print(indexes[0])

