# -*- coding: utf-8 -*-
"""
Created on Tue Jul 5 16:14:04 2016

@author: aleksandr.romanenko@phystech.edu
"""

import os
import unittest

from tools.Collection import Collection
from tools.Collection import Document

class TestDocumentClass(unittest.TestCase):

    def setUp(self):
        self.doc1 = Document()
        self.doc1.content = {
            u'mod1': [1, 2, 3, 4, 1],
            u'mod2': [5, 5, 6, 6],
        }

        self.doc2 = Document()
        self.doc2.content = {
            u'mod1': [1, 2, 3, 4, 1],
            u'mod2': [5, 5, 6, 6],
        }

        self.doc2.indexes = {
            u'mod1': [(0, 2), (2, 1), (4, 6), (12, 2), (15,2)]
        }

    def test_length(self):
        self.assertEqual(self.doc1.length(),9,
                         u"WRONG LENGTH OF DOCUMENT FUNCTION")
        self.assertEqual(self.doc2.length(),9,
                         u"WRONG LENGTH OF DOCUMENT FUNCTION")
        self.assertEqual(self.doc1.length(u'mod1'),5,
                         u"WRONG LENGTH OF DOCUMENT FUNCTION")
        self.assertEqual(self.doc2.length(u'mod1'),5,
                         u"WRONG LENGTH OF DOCUMENT FUNCTION")
        self.assertEqual(self.doc1.length(u'unk_mod'), 0,
                         u"WRONG LENGTH OF DOCUMENT FUNCTION")

    def test_doc_bow(self):
        bow_full = {1: 2, 2: 1, 3: 1, 4: 1, 5: 2, 6: 2}
        bow_mod1 = {1: 2, 2: 1, 3: 1, 4: 1}
        self.assertDictEqual(self.doc1.get_bow(),bow_full,
                             u"WRONG BOW REPRESENTATION")
        self.assertDictEqual(self.doc2.get_bow([u'mod1']),bow_mod1,
                             u"WRONG BOW REPRESENTATION")

    def test_iter_by_doc(self):
        self.assertListEqual(sorted([x for x in self.doc1]), [1, 1, 2, 3, 4, 5, 5, 6, 6],
                             u"PROBLEM WITH ITERATION BY DOCUMENT")

    def test_remove_word(self):
        self.doc1.remove_word(1)
        self.assertListEqual(self.doc1.get('mod1'), [2, 3, 4],
                             u"WRONG REMOVING WORD FUNCTION")
        self.doc2.remove_word(1)
        self.assertListEqual(self.doc2.get('mod1'), [2, 3, 4],
                             u"WRONG REMOVING WORD FUNCTION")
        self.assertListEqual(self.doc2.get_indexes(u'mod1'), [(2, 1), (4, 6), (12, 2)],
                             u"WRONG REMOVING WORD FUNCTION (INDEXES ISSUE)")

    def test_add_word(self):
        self.doc1.add_word(1,u'mod1')
        self.assertListEqual(self.doc1.get(u'mod1'), [1, 2, 3, 4, 1, 1],
                             u"WRONG ADD WORD FUNCTION")

    def test_add_word_and_index(self):
        self.doc2.add_word_and_index(1,u'mod1', (17, 2))
        self.assertListEqual(self.doc2.get(u'mod1'), [1, 2, 3, 4, 1, 1],
                             u"WRONG ADD WORD FUNCTION (WITH INDEXES)")
        self.assertListEqual(self.doc2.get_indexes(u'mod1'), [(0, 2), (2, 1), (4, 6), (12, 2), (15, 2), (17, 2)],
                             u"WRONG ADD WORD FUNCTION (WITH INDEXES)")

    def test_has_word(self):
        self.assertEquals(self.doc1.has_key(1),True,
                          u"WRONG HAS_KEY/HAS_WORD FUNCTION")
        self.assertEquals(self.doc1.has_word(1,u'mod1'),True,
                          u"WRONG HAS_KEY/HAS_WORD FUNCTION")
        self.assertEquals(self.doc1.has_key(1, u'mod2'), False,
                          u"WRONG HAS_KEY/HAS_WORD FUNCTION")


class TestCollectionFunctions(unittest.TestCase):

    def setUp(self):
        self.coll_modalities = Collection()
        self.coll_modalities.verbose = True
        self.coll_modalities.load_split_modality_files(u'test', u'./test_data/modalities/')
        self.coll_UCI = Collection(u'test_data/UCI/vocab.test.txt',
                                      u'test_data/UCI/docword.test.txt')

    def test_collection_indexes(self):
        self.assertListEqual(self.coll_modalities.index_modalities, [u'mod1'],
                             u"WRONG NUMBER OF INDEX MODALITIES")
        self.assertListEqual(self.coll_UCI.index_modalities,[],
                             u"WRONG NUMBER OF INDEX MODALITIES")
        for doc in self.coll_modalities.docword.values():
            self.assertEquals(len(doc.get_indexes(u'mod1')),len(doc.get(u'mod1')))

    def test_collection_lengthes(self):
        self.assertEqual(self.coll_modalities.size(), 5,
                         u"WRONG NUMBER OF DOCS IN COLLECTION (modalities data)")
        self.assertEqual(self.coll_UCI.size(), 5,
                         u"WRONG NUMBER OF DOCS IN COLLECTION (UCI data)")
        self.assertEqual(len(self.coll_UCI.get_document(1, None, 'list')), 7,
                         u"WRONG LENGTH OF DOC (UCI data)")


    def test_vocabularies_lengthes(self):
        self.assertEqual(self.coll_modalities.get_vocabulary().size(), 11,
                         u"WRONG NUMBER OF ELEMENTS IN VOCABULARY (modalities data)")
        self.assertEqual(self.coll_UCI.get_vocabulary().size(), 11,
                         u"WRONG NUMBER OF ELEMENTS IN VOCABULARY (UCI data)")

    def test_modalities_list(self):
        self.assertEqual(sorted(self.coll_modalities.get_vocabulary().modality_list()), [u'mod1', u'mod2'],
                         u"WRONG MODALITY LIST (modalities data)")
        self.assertEqual(sorted(self.coll_UCI.get_vocabulary().modality_list()), [u'mod1',u'mod2'],
                         u"WRONG MODALITY LIST (UCI data)")

    def test_collection_length_in_words(self):
        self.assertEqual(sum([x[0] for x in self.coll_modalities.tf_df(u'mod1').values()]), 16,
                         u"WRONG LENGTH OF COLLECTION IN WORDS (modalities data)")
        self.assertEqual(sum([x[0] for x in self.coll_UCI.tf_df(u'mod1').values()]), 16,
                         u"WRONG LENGTH OF COLLECTION IN WORDS (UCI data)")

    def test_remove_words(self):
        c = self.coll_UCI
        c.remove_words(word_list=[u'c1',u'c2'],modality=u'mod2')
        self.assertEqual(c.get_vocabulary().size(), 9,
                         u"WRONG NUMBER LENGTH OF VOCAB AFTER WORDS REMOVING (UCI data)")

        c = self.coll_modalities
        c.remove_words(word_list=[u'c1',u'c2'],modality=u'mod2')
        self.assertEqual(c.get_vocabulary().size(), 9,
                         u"WRONG NUMBER LENGTH OF VOCAB AFTER WORDS REMOVING (modalities data)")

    def test_remove_all_words_from_doc(self):
        c = self.coll_modalities
        to_remove = list(set(c.get_document(1).get('mod1')))
        c.remove_words(to_remove)
        self.assertEqual(len(c.get_document(1,['mod1'],'list')), 0,
                         u"WRONG NUMBER LENGTH OF VOCAB AFTER WORDS REMOVING (modalities data)")
        self.assertEqual(len(c.get_document(2,['mod1'],'list')), 0,
                         u"WRONG NUMBER LENGTH OF VOCAB AFTER WORDS REMOVING (modalities data)")
        self.assertEqual(len(c.get_document(3,['mod1'],'list')), 0,
                         u"WRONG NUMBER LENGTH OF VOCAB AFTER WORDS REMOVING (modalities data)")
        self.assertEqual(len(c.get_document(4,['mod1'],'list')), 4,
                         u"WRONG NUMBER LENGTH OF VOCAB AFTER WORDS REMOVING (modalities data)")
        self.assertEqual(len(c.get_document(5,['mod1'],'list')), 1,
                         u"WRONG NUMBER LENGTH OF VOCAB AFTER WORDS REMOVING (modalities data)")

        self.assertEqual(len(c.get_document(1,['mod2'],'list')), 1,
                         u"WRONG NUMBER LENGTH OF VOCAB AFTER WORDS REMOVING (modalities data)")
        self.assertEqual(len(c.get_document(2,['mod2'],'list')), 2,
                         u"WRONG NUMBER LENGTH OF VOCAB AFTER WORDS REMOVING (modalities data)")
        self.assertEqual(len(c.get_document(3,['mod2'],'list')), 1,
                         u"WRONG NUMBER LENGTH OF VOCAB AFTER WORDS REMOVING (modalities data)")
        self.assertEqual(len(c.get_document(4,['mod2'],'list')), 1,
                         u"WRONG NUMBER LENGTH OF VOCAB AFTER WORDS REMOVING (modalities data)")
        self.assertEqual(len(c.get_document(5,['mod2'],'list')), 2,
                         u"WRONG NUMBER LENGTH OF VOCAB AFTER WORDS REMOVING (modalities data)")


    def test_remove_documents(self):
        c = self.coll_UCI
        wid = c.get_vocabulary().get_word_id(u'w4',u'mod1')
        doc_id = -1
        for x, doc in c.get_docword().items():
            if doc.has_key(wid):
                doc_id = x
                break
        c.remove_documents([doc_id])
        self.assertEqual(c.get_vocabulary().size(), 10,
                         u"WRONG NUMBER LENGTH OF VOCAB AFTER DOCUMENT REMOVING (UCI data)")

        c = self.coll_modalities
        wid = c.get_vocabulary().get_word_id(u'w4',u'mod1')
        doc_id = -1
        for x,doc in c.get_docword().iteritems():
            if doc.has_key(wid):
                doc_id = x
        c.remove_documents([doc_id])
        self.assertEqual(c.get_vocabulary().size(), 10,
                         u"WRONG NUMBER LENGTH OF VOCAB AFTER DOCUMENT REMOVING (modalities data)")

    def test_filter_by_freq(self):
        self.assertEqual(len(self.coll_modalities.filter_by_freq(u'mod1', 2, 2)), 5,
                         u"WRONG NUMBER OF FILTERED WORDS (modalities data)")
        self.assertEqual(len(self.coll_UCI.filter_by_freq(u'mod1', 2, 2)), 5,
                         u"WRONG NUMBER OF FILTERED WORDS (UCI data)")

if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(TestCollectionFunctions)
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(TestDocumentClass))
    unittest.TextTestRunner(verbosity=5).run(suite)