# -*- coding: utf-8 -*-
"""
Created on Fri Dec  4 13:31:04 2015

@author: aromanenko
"""

import re
import json
import pymorphy2
import networkx
import numpy as np
from scipy.sparse import csc_matrix


class KeyPhraseExtractor(object):
    
    def __init__(self):
        self._windowSize = 5
        self._pymorph = pymorphy2.MorphAnalyzer()
    
    @property
    def windowSize(self):
        return self._windowSize

    @windowSize.setter
    def windowSize(self, value):
        self._windowSize = value
                
    def _buildTransitionMatrix(self,tokenized_doc):
        voc = {}
        for i,x in enumerate(list(set(tokenized_doc))):
            voc[x] = i
        n = len(voc)
        G = np.zeros([n,n],dtype=float)
        for i,x in enumerate(tokenized_doc):
            for j in range(i+1,min(i+self._windowSize,len(tokenized_doc))):
                G[voc[x],voc[tokenized_doc[j]]] = 1.0
#                G[voc[tokenized_doc[j]],voc[x]] += 1.0
        return G, [pair[0] for pair in sorted(voc.items(),key=lambda x: x[1])]
    
    def _buildTransitionMatrix2(self,tokenized_doc):
        voc = {}
        for i,x in enumerate(list(set(tokenized_doc))):
            voc[x] = i
        n = len(voc)
        G = []
        for i,x in enumerate(tokenized_doc):
            for j in range(i+1,min(i+self._windowSize,len(tokenized_doc))):
                G.append( (voc[x],voc[tokenized_doc[j]]) )
#                G[voc[tokenized_doc[j]],voc[x]] += 1.0
        return sorted(list(set(G))), [pair[0] for pair in sorted(voc.items(),key=lambda x: x[1])]


    def _buildTransitionMatrix3(self,tokenized_doc):
        voc = {}
        i = 0
        for x in list(set(tokenized_doc)):
            flag = False
#            for w in x.split(u' '):
#                if self._pymorph.parse(w)[0].tag.POS not in [u'NOUN', u'ADJF']:
#                    flag = True
#                    break
            if flag:
                continue
            voc[x] = i
            i += 1
        n = len(voc)
        mat = np.zeros([n,n],dtype=float)
        for i,w1 in enumerate(tokenized_doc):
            if w1 not in voc:
                continue
            for j in range(i+1,min(i+self._windowSize,len(tokenized_doc))):
                w2 = tokenized_doc[j]
                if w2 not in voc:
                    continue
#                G[voc[x],voc[tokenized_doc[j]]] = 1.0
                mat[voc[w1],voc[w2]] += 1.0
                mat[voc[w2],voc[w1]] += 1.0
        G = networkx.Graph()
        G.add_nodes_from(voc.values())
        for i in range(len(voc)):
            for j in range(len(voc)):
                G.add_weighted_edges_from([(i,j,mat[i,j]),(j,i,mat[i,j])])
#        for i,w1 in enumerate(tokenized_doc):
#            if w1 not in voc:
#                continue
#            for j in range(i+1,min(i+self._windowSize,len(tokenized_doc))):
#                w2 = tokenized_doc[j]
#                if w2 not in voc:
#                    continue
#                G.add_edge( voc[w1],voc[w2] )

        return G, [pair[0] for pair in sorted(voc.items(),key=lambda x: x[1])]
    
    def _pagerank(graph, damping=0.85, epsilon=1.0e-8):
        inlink_map = {}
        outlink_counts = {}
        
        def new_node(node):
            if node not in inlink_map: inlink_map[node] = set()
            if node not in outlink_counts: outlink_counts[node] = 0
        
        for tail_node, head_node in graph:
            new_node(tail_node)
            new_node(head_node)
            if tail_node == head_node: continue
            
            if tail_node not in inlink_map[head_node]:
                inlink_map[head_node].add(tail_node)
                outlink_counts[tail_node] += 1
        
        all_nodes = set(inlink_map.keys())
        for node, outlink_count in outlink_counts.items():
            if outlink_count == 0:
                outlink_counts[node] = len(all_nodes)
                for l_node in all_nodes: inlink_map[l_node].add(node)
        
        initial_value = 1 / len(all_nodes)
        ranks = {}
        for node in inlink_map.keys(): ranks[node] = initial_value
        
        new_ranks = {}
        delta = 1.0
        n_iterations = 0
        while delta > epsilon:
            new_ranks = {}
            for node, inlinks in inlink_map.items():
                new_ranks[node] = ((1 - damping) / len(all_nodes)) + (damping * sum(ranks[inlink] / outlink_counts[inlink] for inlink in inlinks))
            delta = sum(abs(new_ranks[node] - ranks[node]) for node in new_ranks.keys())
            ranks, new_ranks = new_ranks, ranks
            n_iterations += 1
        
        return ranks, n_iterations
    
    def extract(self,tokenized_doc):
        G,voc = self._buildTransitionMatrix3(tokenized_doc)
        r = networkx.pagerank(G)
        return r,voc
        
    def improveView(self, ranks, voc, top_n=10):
        TRESHHOLD = 0.05
        best = [pair[0] for pair in sorted(ranks.items(),key=lambda x: x[1])[::-1]]
        keywords = [(voc[x],ranks[x]) for x in best[:10] if voc[x].count(' ') > 0]
        for x in best[:10]:
            if ranks[x] < TRESHHOLD:
                break
            keywords += [(voc[x],ranks[x])]
        if not keywords:
            keywords += [(voc[best[0]],ranks[best[0]])]
        keywords = list(set(keywords))
        keywords = [(self._incline(x[0]),x[1]) for x in keywords if self._incline(x[0])]
        return keywords
        
    def _incline(self,kw):
        tags = [self._pymorph.parse(w)[0].tag.POS for w in kw.split(u' ')]
        tags = u' '.join(tags)
        words = kw.split(u' ')
        if tags == u'NOUN':
            return self._pymorph.parse(kw)[0].normal_form
        if tags == u'ADJF NOUN':
            w = self._pymorph.parse(words[1])[0]
            case, gender = w.tag.case, w.tag.gender
            w1 = self._pymorph.parse(words[0])[0].inflect({case,gender}).word
            w2 = w.normal_form
            res = w1+u' '+w2
        elif tags == u'NOUN NOUN':
            w1 = words[0]
            w2 = self._pymorph.parse(words[1])[0].inflect({'gent'}).word
            res = w1+u' '+w2
        elif tags == u'ADJF ADJF NOUN':
            w = self._pymorph.parse(words[2])[0]
            case, gender = w.tag.case, w.tag.gender
            w1 = self._pymorph.parse(words[0])[0].inflect({case,gender}).word
            w2 = self._pymorph.parse(words[1])[0].inflect({case,gender}).word
            w3 = w.normal_form
            res = w1+u' '+w2+u' '+w3
        elif tags == u'ADJF ADJF NOUN':
            w = self._pymorph.parse(words[1])[0]
            case, gender = w.tag.case, w.tag.gender
            w1 = self._pymorph.parse(words[0])[0].inflect({case,gender}).word
            w2 = self._pymorph.parse(words[1])[0].inflect({case,gender}).word
            w3 = w.normal_form
            res = w1+u' '+w2+u' '+w3
        else:
            res = None
        return res
            
    def makePhrases(self,ranks,voc,tokenized_doc,top_n=10):
        best = [p[0] for p in sorted(ranks.items(),key=lambda x: x[1])[::-1]]
        doc = ' '.join(tokenized_doc)
        res = []
        to_remove_idx = []
        for i in best[:top_n]:
            for j in best[:top_n]:
                if i == j or i in to_remove_idx or j in to_remove_idx:
                    continue
                count = doc.count(voc[i]+' '+voc[j])
                if (count):
                    res.append(([i,j],count))
                    to_remove_idx.append(i)
                    to_remove_idx.append(j)
                    
        res = sorted(res,key=lambda x: x[1])[::-1]
        new_ranks = {}
        for el in best[:top_n]:
            if el in to_remove_idx:
                continue
            new_ranks[voc[el]] = ranks[el]
        for el in res:
            new_ranks[' '.join([voc[i] for i in el[0]])] = sum([ranks[i] for i in el[0]])
        return new_ranks
        
        

if __name__ == "__main__":
#    doc = corpora[505]
#    doc = ng.ngramm(doc,2)
#    doc = ng.removeDelimiters(doc)
#    kpd = KeyPhraseExtractor()    
#    kpd.windowSize = 10
#    ranks,voc = kpd.extract(doc)
#    nr = kpd.improveView(ranks,voc)
    with open('log2.txt','w') as f:
        for idx in range(500,600):
            doc = corpora[idx]
            doc = ng.ngramm(doc,2)
            doc = ng.removeDelimiters(doc)
        
            kpd = KeyPhraseExtractor()
            
            kpd.windowSize = 5
        #    G,voc = kpd._buildTransitionMatrix2(doc)
        #    ranks = networkx.pagerank(G)
        #    ranks, n = pagerank(G)
            ranks,voc = kpd.extract(doc)
#            nr = kpd.improveView(ranks,voc)
#            best = sorted(nr,key=lambda x: x[1])[::-1]
            #            nr = kpd.makePhrases(ranks,voc,doc)
            best = [pair[0] for pair in sorted(ranks.items(),key=lambda x: x[1])[::-1]]
        #    best = np.argsort(r)[::-1]
            f.write(50*'-'+'\n')
            f.write((u' '.join(doc[:100])).encode('utf-8'))
            f.write('\n\n')
            for i in best[:10]:
#                f.write(u'{:<30} {:.4f}\n'.format(i[0],i[1]).encode('utf-8'))
                f.write(u'{:<30} {:.4f}\n'.format(voc[i],ranks[i]).encode('utf-8'))
#                f.write(u'{:<30} {:.4f}\n'.format(i,nr[i]).encode('utf-8'))