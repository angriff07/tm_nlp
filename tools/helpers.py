# -*- coding: utf-8 -*-
"""
Created on Fri Dec  4 13:31:04 2015

@author: aromanenko
"""

import os
import glob
import json
import base64
import codecs
import struct
import numpy as np
import pandas as pd
import sklearn.metrics

from matplotlib import cm
import matplotlib.pyplot as plt
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from mpl_toolkits.mplot3d import Axes3D

import artm
from artm.batches_utils import Batch

def trainModel(batch_vectorizer, num_topics, class_ids, regularizers_list,scores_list,regularizers_tau=None,iterNum = 40,num_document_passes=2):
    if regularizers_tau == None:
        regularizers_tau = {}
    model = artm.ARTM(num_processors=3,num_topics = num_topics, class_ids=class_ids,
                      num_document_passes=num_document_passes)
    if 'dictionary.dict' not in os.listdir(batch_vectorizer.data_path):
        dictionary = artm.Dictionary()
        dictionary.gather(data_path=batch_vectorizer.data_path)
        dictionary.save(os.path.join(batch_vectorizer.data_path,'dictionary.dict'))
    else:
        dictionary = artm.Dictionary(dictionary_path=os.path.join(batch_vectorizer.data_path,'dictionary.dict'))
    model.initialize(dictionary=dictionary)
#    model.load_dictionary('dictionary',os.path.join(batch_vectorizer.data_path,'dictionary'))
#    model.initialize(data_path=batch_vectorizer.data_path)
    for reg in regularizers_list:
        model.regularizers.add(reg)
    for score in scores_list:
        model.scores.add(score)
    for i in range(iterNum):
        for reg in regularizers_tau.keys():
            model.regularizers[reg].tau = regularizers_tau[reg][i]
        model.fit_offline(batch_vectorizer, num_collection_passes=1)
    return model

def trainModel2(model,batch_vectorizer, num_topics, class_ids, regularizers_tau=None,iterNum = 40,
                num_document_passes=2, retrain = False):
    if regularizers_tau == None:
        regularizers_tau = {}
    if not retrain:
        if 'dictionary.dict' in os.listdir(batch_vectorizer.data_path):
            model.load_dictionary(dictionary_name='dictionary',
                                  dictionary_path=os.path.join(batch_vectorizer.data_path,'dictionary.dict'))
        else:
            model.gather_dictionary(dictionary_target_name='dictionary',data_path=batch_vectorizer.data_path)
            model.save_dictionary(dictionary_name='dictionary',
                                  dictionary_path=os.path.join(batch_vectorizer.data_path,'dictionary.dict'))
        model.initialize(dictionary_name='dictionary')
    else:
        model.num_topics = num_topics
        model.class_ids = class_ids

    for i in range(iterNum):
        for reg in regularizers_tau.keys():
            model.regularizers[reg].tau = regularizers_tau[reg][i]
        model.fit_offline(batch_vectorizer, num_collection_passes=1,num_document_passes=num_document_passes)
    return
    
def print_measures(model,score_types=None, nonPrint=True):
    scores = model.score_tracker.keys()
    if score_types == None:
        score_types = [0,1,2,6]
    retVal = {}
    for t in score_types:
        l = []
        for score in scores:
            if model.scores[score].type == t:
                l.append(score)
        toPrint = ''
        for score in sorted(l):
            if t in [0,1]:
                toPrint = score+': {0:.3f}'.format(model.score_tracker[score].last_value)
                retVal[score] = model.score_tracker[score].last_value
            if t in [2]:
                toPrint = score+': {0:.3f} ('.format(model.score_tracker[score].last_value)+model.scores[score].class_id+')'
                retVal[score] = model.score_tracker[score].last_value
            if t in [6]:
                toPrint = 'Kernel properties ('+model.scores[score].class_id+'): '
                toPrint += 'Contrast = {0:.3f}, '.format(model.score_tracker[score].last_average_contrast)
                toPrint += 'Purity = {0:.3f}, '.format(model.score_tracker[score].last_average_purity)
                toPrint += 'Size = {0:.2f}'.format(model.score_tracker[score].last_average_size)
                retVal[score+'.contrast'] = model.score_tracker[score].last_average_contrast
                retVal[score+'.purity'] = model.score_tracker[score].last_average_purity
                retVal[score+'.size'] = model.score_tracker[score].last_average_size
            if nonPrint:
                print toPrint
    return retVal
            
    
def get_top_words(model):
    topTokenScores = []
    for score in model.score_tracker.keys():
        if model.scores[score].type == 4:
            topTokenScores.append(score)
    toPrint = {}
    for score in topTokenScores:
        last_tokens = model.score_tracker[score].last_tokens
        last_weights = model.score_tracker[score].last_weights
        for topic_name in last_tokens:
            toPrint.setdefault(topic_name, {})
            toPrint[topic_name].setdefault(model.scores[score].class_id, {})
            if isinstance(last_weights[topic_name],list):
                toPrint[topic_name][model.scores[score].class_id]['words'] = [x for x in last_tokens[topic_name]]
                toPrint[topic_name][model.scores[score].class_id]['weights'] = last_weights[topic_name]
            else:
                toPrint[topic_name][model.scores[score].class_id]['words'] = [last_tokens[topic_name]]
                toPrint[topic_name][model.scores[score].class_id]['weights'] = [last_weights[topic_name]]
    return toPrint


def show_words(model, with_probabilities=False, sorting_func=None):
    if sorting_func is None:
        sorting_func = lambda x: int(x.split(u"_")[-1])

    toPrint = get_top_words(model)

    for t in sorted(toPrint.keys(), key=sorting_func):
    	v = toPrint[t]
        print t+u":\n",
        for score,vectors in v.iteritems():
            print score+u':',
            s = u''
            if with_probabilities:
                s += u', '.join([word+u'({0:.3f})'.format(vectors['weights'][i]) for i,word in enumerate(vectors['words'])])
            else:
                s += u', '.join(vectors['words'])
            print s+u'\n',
        print ''



def plot_graphs(model,score_types=None):
    colors = ['b','r','g','k']
    styles = ['--','-.','-']
    scores = model.score_tracker.keys()
    if score_types == None:
        score_types = [[0],[1,2],[6]]
    for t in score_types:
        l = []
        for score in scores:
            if model.scores[score].type in t:
                l.append(score)
        c_iter = -1
        for score in sorted(l):
            s_iter = 0
            c_iter = (c_iter+1) % len(colors)
            if model.scores[score].type in [0]:
                plt.plot(xrange(model.num_phi_updates), model.score_tracker[score].value, linewidth=2)
                plt.ylabel(score)
                
            if model.scores[score].type in [6]:
                plt.figure(1)
                plt.subplot(211)
                plt.plot(xrange(model.num_phi_updates), model.score_tracker[score].average_contrast, colors[c_iter]+styles[s_iter], linewidth=2,label='Contrast '+model.scores[score].class_id)
                s_iter = (s_iter+1) % len(styles)
                plt.plot(xrange(model.num_phi_updates), model.score_tracker[score].average_purity, colors[c_iter]+styles[s_iter], linewidth=2,label='Purity '+model.scores[score].class_id)
                plt.ylabel('Contrast, Purity')
                plt.grid(True)
                plt.legend(loc='best',prop={'size':9})

                plt.subplot(212)
                plt.plot(xrange(model.num_phi_updates), model.score_tracker[score].average_size, colors[c_iter]+styles[s_iter], linewidth=2,label=model.scores[score].class_id)
                plt.ylabel('Kernel size')
                plt.legend(loc='best')


            if model.scores[score].type in [1,2]:
                plt.plot(xrange(model.num_phi_updates), model.score_tracker[score].value, colors[c_iter]+styles[s_iter], linewidth=2,label=score)
                plt.ylabel('Sparsity')
                plt.legend(loc='best')

        plt.xlabel('Iterations')
        plt.grid(True)
        plt.show()
    
def prepareTable(QM,categories):
    html = ["<table width=100%>"]
    html.append("<tr>")
    html.append("<td>{0}</td>".format("Category name"))
    for cat in categories:
        html.append("<td>{0}</td>".format(cat))
    html.append("</tr>")
    
    for key in ['F1','P','R']:
        value = QM[key]
        html.append("<tr>")
        html.append("<td>{0}</td>".format(key))
        for i in range(len(categories)):
            html.append("<td>{0:.3f}</td>".format(value[i]))
        html.append("</tr>")
    html.append("</table>")
    return ''.join(html)


def printQuality(QM):
    print 'Number of classified objects: {0}'.format(np.sum(QM['supported']))
    print 'Weighted F1: {0:.3f}'.format(np.sum(QM['F1_weighted']))
    print 'F1 macro: {0:.3f}'.format(np.sum(QM['F1_macro']))
    print 'Macro Precision: {0:.3f}'.format(np.sum(QM['P'])/len(QM['F1']))
    print 'Macro Recall: {0:.3f}'.format(np.sum(QM['R'])/len(QM['F1']))
    print 'Accuracy of Top1 answer: {0:.3f}'.format(QM['ACC1'])


def printAnswers(categories,doc_ids,p_cd,true_answers):
    ind = np.argsort(p_cd,axis=0)[len(categories)-3:,:]
    for i in range(np.size(p_cd,1)):
        print doc_ids[i],u'('+true_answers[i]+u'):',
        for cat in ind[:,i][::-1]:
            print categories[cat],u'('+unicode(p_cd[cat,i])+u')',
        print u'\n'


def loadFileAsStringArray(filename, empty_strings=False):
    content = []
    with open(filename, 'r') as f:
        for line in f:
            content += [line.decode('utf-8').strip(u'\n\r\0')]
    if not empty_strings:
        content = [line for line in content if len(line)]
    return content


def classify(model,batch_vectorizer,num_document_passes=1, predict_class_id='category'):
    _num_document_passes = model.num_document_passes
    model.num_document_passes = num_document_passes
    res=model.transform(batch_vectorizer=batch_vectorizer, predict_class_id=predict_class_id)
    doc_ids = sorted(res.columns.values)
    p_cd = res.ix[:,doc_ids].as_matrix()
    categories = list(res.index)
    model.num_document_passes = _num_document_passes
    return categories,doc_ids,p_cd


def findTheta(model,batch_vectorizer,num_document_passes=1, alpha = 0.05):
    topic_size = len(model.topic_names)
    doc_ids = []
    phi = model.get_phi(class_ids=["ru"])
    res = []
    for b in batch_vectorizer.batches_list:
        batch = artm.messages.Batch()
        with open(b.filename, "rb") as f:
            batch.ParseFromString(f.read())
        for item in batch.item:
            doc_ids.append(item.id)
            field = item.field[0]
            theta = np.ones((1, topic_size)) / topic_size
#            nd = sum(field.token_weight)

            for i in xrange(num_document_passes):
                ntd = np.zeros((1, topic_size))
                for token_id, token_count in zip(field.token_id, field.token_weight):
                    token = batch.token[token_id]
                    if token in phi.index:
                        pwt = phi[token:token].values[0,:]

                        zw = sum(sum(theta * pwt))
                        if zw == 0:
                            continue
                        a = token_count / zw
                        ntd = ntd + a * pwt

                theta = theta * ntd
                theta = theta + alpha
                theta = np.maximum(theta, 0)
                theta = theta / sum(sum(theta))
            res.append(theta)
    return res

def top_n_accuracy(categories,p_cd,true_answers, n = 3):
    ACC = 0.0
    separate = {}
    number = {}
    for x in categories:
        separate[x] = 0.0
        number[x] = 0.0
    p = np.argsort(p_cd,axis=0)[len(categories)-n:,:]
    for i in range(np.size(p,1)):
        for x in p[:,i][::-1]:
            if categories[x] == true_answers[i]:
                separate[true_answers[i]] += 1
                ACC += 1
        number[true_answers[i]] += 1
    for key in separate:
        separate[key] /= number[key]
    res = {'ACC': ACC/len(true_answers), 'R':separate}
    return res

def multilabel_quality(categories,p_cd,true_answers, num=3 , p_treshhold=0.0):
    R = 0.0
    P = 0.0
    MAX_PR = 0.0
    pcd = np.argsort(p_cd,axis=0)
    for i in range(np.size(pcd,1)):
        true_answ = true_answers[i]
        pred_answ = []
        for j,x in enumerate(pcd[:,i][::-1]):
            if  p_cd[x,i] < p_treshhold or j >= num:
                break
            pred_answ += [categories[x]]
        tp = sum([1.0 for answ in pred_answ if answ in true_answ])
        r = tp/len(true_answ)
        if len(pred_answ):
            p = tp/len(pred_answ)
        else:
            p = 0.0
        R += r
        P += p
        MAX_PR += max([p,r])
    return {"P": P/len(true_answers), 
            "R": R/len(true_answers), 
            "MAX_PR": MAX_PR/len(true_answers)}

def labeling(model, batch_vectorizer, ptd_treshhold=0.01, ptdw_treshhold=0.01):
    top_words = {}
    for i, batch_name in enumerate([b.filename for b in batch_vectorizer.batches_list]):
        if i > 0:
            break
        bv = artm.BatchVectorizer(batches=[batch_name])
        ptdw = model.transform(bv, theta_matrix_type='dense_ptdw')       
        ptd = model.transform(bv)
        batch = artm.messages.Batch()
        with open(batch_name, "rb") as f:
            batch.ParseFromString(f.read())
        for doc in batch.item:
            topics = []
            for topic in ptd.sort_values(by=doc.id, ascending=False).index[1:]:
                if ptd.ix[topic, doc.id] > ptd_treshhold:
                    topics.append(topic)
            trmd_ptdw = ptdw.ix[topics,doc.id]
            for t in trmd_ptdw.index:
                for i,x in enumerate(trmd_ptdw.ix[t,:]):
                    if x > ptdw_treshhold:
                        top_words.setdefault(t,{})
                        top_words[t].setdefault(batch.token[doc.token_id[i]],{'count':0, 'prob':0})
                        top_words[t][batch.token[doc.token_id[i]]]['count'] += 1
                        top_words[t][batch.token[doc.token_id[i]]]['prob'] += x
    return top_words

def quality(categories,p_cd,true_answers):
    pred_answ = np.argsort(p_cd,axis=0)[len(categories)-1:,:][0]
    true_answ = [categories.index(i) for i in true_answers]
    P = sklearn.metrics.precision_recall_fscore_support(true_answ, pred_answ)[0]
    R = sklearn.metrics.precision_recall_fscore_support(true_answ, pred_answ)[1]
    F1 = sklearn.metrics.precision_recall_fscore_support(true_answ, pred_answ)[2]
    supported = sklearn.metrics.precision_recall_fscore_support(true_answ, pred_answ)[3]
    ACC1 = sklearn.metrics.accuracy_score(true_answ, pred_answ)
    F1_micro = sklearn.metrics.f1_score(true_answ, pred_answ, average='micro')
    F1_macro = sklearn.metrics.f1_score(true_answ, pred_answ, average='macro')
    F1_weighted = sklearn.metrics.f1_score(true_answ, pred_answ, average='weighted')
    return {'F1':F1,'P':P,'R':R,'ACC1':ACC1, 'F1_micro':F1_micro,'F1_macro':F1_macro,'F1_weighted':F1_weighted,'supported':supported,'ConfMat':sklearn.metrics.confusion_matrix(true_answ, pred_answ)}

def qualityConfusionMatrix(QM,categories):
    return pd.DataFrame(QM['ConfMat'], index=categories, columns=categories)
    
def confidentAnswers(categories,doc_ids,p_cd,true_answers, trshld1 = 0, trshld2 = 0.2):
    result = {}
    confMat = np.zeros((len(categories),len(categories)))
    unconf_ans = np.zeros(len(categories))
    ind = np.argsort(p_cd,axis=0)[len(categories)-3:,:]
    for i in range(np.size(p_cd,1)):
        ind_sort = ind[:,i][::-1]
        if p_cd[ind_sort[0],i] > p_cd[ind_sort[1],i]+trshld2 and p_cd[ind_sort[0],i] > trshld1:
            confMat[ind_sort[0],categories.index(true_answers[i])] += 1
        else:
            unconf_ans[categories.index(true_answers[i])] += 1
    tp_fp = np.sum(confMat,axis=1)
    tp_fn = np.sum(confMat,axis=0)
    tp = np.diag(confMat)
    P = map(lambda x,y: x/y if y>0 else 0, tp,tp_fp)
    R = map(lambda x,y: x/y if y>0 else 0, tp,tp_fn)
    F1 = map(lambda x,y: 2*x*y/(x+y) if (x+y)>0 else 0, P,R)
    avg_F1 = sum(F1)/len(F1)
    avg_ratio = sum(tp_fn)/sum(unconf_ans+tp_fn)
    result['confMat'] = confMat
    result['nonconf_answ'] = unconf_ans
    result['P'] = P
    result['R'] = R
    result['F1'] = F1
    result['avg_ratio'] = avg_ratio
    result['avg_F1'] = avg_F1
    return result    

def plot3d(X,Y,Z,xlabel,ylabel,zlabel,title = None,figsize=(15, 10),dpi=80):
    fig = plt.figure(figsize=figsize, dpi=dpi)
    ax = fig.gca(projection='3d')
    delta_x = (np.max(X)-np.min(X))/50.0
    delta_y = (np.max(Y)-np.min(Y))/50.0
    ax.set_xlim(np.min(X)-delta_x, np.max(X)+delta_x)
    ax.set_ylim(np.min(Y)-delta_y, np.max(Y)+delta_y)
    X_,Y_ = np.meshgrid(X,Y)
    surf = ax.plot_surface(X_, Y_, Z, rstride=1, cstride=1, cmap=cm.coolwarm,
                           linewidth=0, antialiased=True)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_zlabel(zlabel)
    if title:
        ax.set_title(title)
    ax.zaxis.set_major_locator(LinearLocator(10))
    ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))
    fig.colorbar(surf, shrink=0.5, aspect=5)
    plt.show()

def n_max(arr, n):
    indices = arr.ravel().argsort()[-n:]
    indices = (np.unravel_index(i, arr.shape) for i in indices)
    return [(arr[i], i) for i in indices]

def topNmodels(mat,n,catWeights,topicNum, labels):
    ind = n_max(mat,n)[::-1]
    table = np.zeros([n,5],dtype=float)
    i = 0
    for x in ind:
        table[i,0] = x[1][0]
        table[i,1] = x[1][1]
        table[i,2] = catWeights[x[1][0]]
        table[i,3] = topicNum[x[1][1]]
        table[i,4] = x[0]
        i+=1
    columns = ['Row index','Column index',labels[0],labels[1],'Quality']
    return pd.DataFrame(table, index=np.arange(1,int(n+1),1).tolist(), columns=columns)
    
class NumpyEncoder(json.JSONEncoder):

    def default(self, obj):
        """If input object is an ndarray it will be converted into a dict 
        holding dtype, shape and the data, base64 encoded.
        """
        if isinstance(obj, np.ndarray):
            if obj.flags['C_CONTIGUOUS']:
                obj_data = obj.data
            else:
                cont_obj = np.ascontiguousarray(obj)
                assert(cont_obj.flags['C_CONTIGUOUS'])
                obj_data = cont_obj.data
            data_b64 = base64.b64encode(obj_data)
            return dict(__ndarray__=data_b64,
                        dtype=str(obj.dtype),
                        shape=obj.shape)
        # Let the base class default method raise the TypeError
        return json.JSONEncoder(self, obj)


def json_numpy_obj_hook(dct):
    """Decodes a previously encoded numpy ndarray with proper shape and dtype.

    :param dct: (dict) json encoded ndarray
    :return: (ndarray) if input was an encoded ndarray
    """
    if isinstance(dct, dict) and '__ndarray__' in dct:
        data = base64.b64decode(dct['__ndarray__'])
        return np.frombuffer(data, dct['dtype']).reshape(dct['shape'])
    return dct

def saveJsonToFile(obj,filename):
    with codecs.open(filename, mode='w', encoding='utf-8') as f:
        json.dump(obj, f, cls=NumpyEncoder)

def loadJsonFromFile(filename):
    with codecs.open(filename,encoding='utf-8') as f:
        obj = json.load(f, object_hook=json_numpy_obj_hook)
    return obj

def saveIndexAsVocabulary(filename,index):
    with codecs.open(filename,mode='w',encoding='utf-8') as f:
        for word in index:
            f.write(word+u'\n')

def saveAsSparse(filename,mat, int_size, float_size):
    mul = 65535
    int_type = 'i'
    float_type = 'd'
    if int_size == 2:
        int_type = 'H'
    if float_size == 4:
        float_type = 'f'
    if float_size == 2:
        mat[mat<1.0/mul] = 0
    with open(filename,'wb') as f:
        f.write(struct.pack(int_type,np.size(mat,0))[::-1])
        f.write(struct.pack(int_type,np.size(mat,1))[::-1])
        for i in range(np.size(mat,0)):
            f.write(struct.pack(int_type,i)[::-1])
            count = 0
            for j in range(np.size(mat,1)):
                if mat[i,j]:
                    count += 1
            f.write(struct.pack(int_type,count)[::-1])
            for j in range(np.size(mat,1)):
                if mat[i,j]:
                    f.write(struct.pack(int_type,j)[::-1])
                    if float_size == 2:
                        f.write(struct.pack(int_type,int(mul*mat[i,j]))[::-1])
                    else:
                        f.write(struct.pack(float_type,mat[i,j])[::-1])

def saveClassIds(filename,class_ids):
    with codecs.open(filename, mode='w', encoding="utf-8") as f:
        for key in class_ids:
            f.write(key+' '+unicode(class_ids[key])+u'\n')
                    
                
def saveModel(model, folder, int_size=4, float_size=8):
    if model.class_ids:
	    saveClassIds(os.path.join(folder,'modalities.txt'),model.class_ids)
	    for class_id in model.class_ids:
	        phi = model.get_phi(class_ids=[class_id])
	        saveIndexAsVocabulary(os.path.join(folder,class_id+'_vocabulary.txt'),phi.index)
	        saveAsSparse(os.path.join(folder,class_id+'_phi.bin'),phi.as_matrix(),int_size=int_size, float_size=float_size)
    else:
        class_id = 'default'
        with codecs.open(os.path.join(folder,'modalities.txt'),mode='w', encoding='utf-8') as f:
            f.write(class_id+u' 1\n')
        phi = model.get_phi()
        saveIndexAsVocabulary(os.path.join(folder,class_id+'_vocabulary.txt'),phi.index)
        saveAsSparse(os.path.join(folder,class_id+'_phi.bin'),phi.as_matrix(),int_size=int_size, float_size=float_size)


def loadExtendedDoc(filename):
    doc = dict()
    text = list()
    with codecs.open(filename,mode='r',encoding='utf-8') as f:
        for line in f:
            if not line:
                continue
            line = line.strip(u'\r\n')
            if line == u"!!!TITLE!!!":
                key = line
                continue
            if line == u"!!!URL!!!":
                key = line
                continue
            if line == u"!!!CATEGORY!!!":
                key = line
                continue
            if line == u"!!!SUBCATEGORY!!!":
                key = line
                continue
            if line == u"!!!IMAGE!!!":
                key = line
                continue
            if line == u"!!!TAGS!!!":
                key = line
                continue
            if line == u"!!!AUTHOR!!!":
                key = line
                continue
            if line == u"!!!DESCRIPTION!!!":
                key = line
                continue
            if line == u"!!!TEXT!!!":
                key = line
                continue
            if line == u"!!!TIME!!!":
                key = line
                doc[key] = list()
                continue
            if key == u"" or key == u"!!!TEXT!!!":
                key = u"!!!TEXT!!!"
                text.append(line)
            else:
                doc[key]=line
        doc[u'!!!TEXT!!!'] = text
    return doc

   
class SingleBatchVectorizer(object):
    def __init__(self, batch_filename, data_path, batch_size):
        self.data_path = data_path
        self.batch_size = batch_size
        self._batch_filename = [Batch(batch_filename)]
        self._batches_list = [Batch(batch_filename)]
        self._weights = [1.0]

    @property
    def batches_list(self):
        return self._batches_list

    @property
    def weights(self):
        return self._weights

    @property
    def num_batches(self):
        return len(self._batches_list)