# -*- coding: utf-8 -*-
"""
Created on Tue Nov 10 15:19:32 2015

@author: angriff07@gmail.com
"""
import os
import glob
import copy
import codecs
import numpy as np


class Vocabulary(object):
    """
    Vocabulary class. It is assumed Vocabulary is member of Collection object.

    It provides support of basic operations with multimodal vocabulary.
    Default name of modality is "word".
    """

    def __init__(self, voc_filename=None):
        """
        Constructor of class instance.
        voc_filename - path to file with vocabulary (UCI vocabulary). Each line of file is one element of vocabulary.
        The last token (last word devided by symbol space) will be taken as name of modality
        Default value is None

        If voc_filename is None, the impty vocabulary will be created 
        """
        self._voc = {}
        self._word2id = {}
        self._id2modality = {}
        if voc_filename is None:
            return
        with codecs.open(voc_filename, encoding='utf-8') as f:
            count = 1
            for line in f:
                line = line.strip(u'\r\n\0')
                splt = line.split(u' ')
                modality = u'word'
                if len(splt) > 1:
                    modality = splt[-1]
                self._voc.setdefault(modality, {})
                self._word2id.setdefault(modality, {})
                self._word2id[modality][u' '.join(splt[:-1])] = count
                self._voc[modality][count] = u' '.join(splt[:-1])
                self._id2modality[count] = modality
                count += 1

    def modality_list(self):
        """Returns list of modalities saved in Vocabulary"""
        return sorted(self._voc.keys())

    def modality_voc(self, modality):
        """Returns vocabulary for specific modality"""
        return self._voc[modality]

    def get_word(self, word_id):
        mod = self._id2modality[word_id]
        return [self._voc[mod][word_id], mod]

    def get_word_id(self, word, modality):
        return self._word2id[modality].get(word,-1)

    def size(self):
        """
        Returns number of entities in vocabulary (over all modalities)
        """
        return len(self._id2modality)


class Document(object):
    """
    Class for supporting storing and accessing to document
    """
    def __init__(self):
        self._content = {}
        self._indexes = {}

    @property
    def content(self):
        return self._content

    @content.setter
    def content(self, value):
        self._content = value

    @property
    def indexes(self):
        return self._indexes

    @indexes.setter
    def indexes(self, value):
        self._indexes = value

    def get_bow(self, modalities=None):
        if modalities is None:
            modalities = self.content.keys()
        docBOW = {}
        for mod in modalities:
            for wid in self.content.get(mod,[]):
                docBOW.setdefault(wid,0)
                docBOW[wid] += 1
        return docBOW

    def get(self,modality):
        return self.content.get(modality,[])

    def add_word(self, word_id, modality):
        self.content.setdefault(modality,[])
        self.content[modality] += [word_id]

    def has_word(self, word_id, modality=None):
        if modality is None:
            mod_list = self.content.keys()
        else:
            mod_list = [modality]
        for mod in mod_list:
            if word_id in self.content.get(mod,[]):
                return True
        return False

    def has_key(self, word_id, modality=None):
        """The same as has_word function (for legacy code using)"""
        return self.has_word(word_id, modality)

    def __iter__(self):
        word_ids = []
        for mod in self.content:
            word_ids += self.content[mod]
        return iter(word_ids)

    def get_indexes(self, modality):
        return self.indexes.get(modality,[])

    def remove_word(self, word_id, modality=None):
        if modality is None:
            mod_list = self.content.keys()
        else:
            mod_list = [modality]
        for mod in mod_list:
            to_remove = [i for i,wid in enumerate(self.content.get(mod,[])) if wid == word_id]
            if not to_remove:
                continue
            if self.content.has_key(mod):
                self.content[mod] = [wid for wid in self.content[mod] if wid != word_id]
            if self.get_indexes(mod):
                self.indexes[mod] = [x for i, x in enumerate(self.indexes[mod]) if i not in to_remove]

    def add_word_and_index(self,word_id,modality, index_pair):
        self.add_word(word_id,modality)
        self.indexes.setdefault(modality,[])
        self.indexes[modality] += [index_pair]

    def length(self,modality=None):
        if modality is None:
            mod_list = self.content.keys()
        else:
            mod_list = [modality]
        length = 0
        for mod in mod_list:
            length += len(self.content.get(mod,[]))
        return length


class Collection(object):
    """
    Class for supporting basic function for operations with collection:
        trimmming Vocabulary and Documents, statistics counting, saving, etc.
    """

    def __init__(self, voc_filename=None, docword_filename=None):
        """
        Constructor of Collection class

        voc_filename - path to file with vocabulary (UCI vocabulary). Each line of file is one element of vocabulary.
            The last token (last word devided by symbol space) will be taken as name of modality
            Default value is None

        docword_filename - path to file with information about bag-of-word content of collection
            (vanilla UCI format of text collections)
            Default value is None

        If voc_filename is None and docword_filename is None then the impty collection with empty vocabulary will be created
        """
        self._verbose = False
        self._index_modalities = []
        self._vocabulary = Vocabulary()
        self._docword = {}
        if voc_filename is None and docword_filename is None:
            return
        self._vocabulary = Vocabulary(voc_filename)
        with codecs.open(docword_filename, encoding='utf-8') as f:
            line_num = 0
            for line in f:
                line_num += 1
                if line_num <= 3:
                    continue
                line = line.strip(u'\r\n\0')
                [doc_id, word_id, count] = [int(x) for x in line.split(u' ')]
                self._docword.setdefault(doc_id, Document())
                for i in xrange(count):
                    self._docword[doc_id].add_word(word_id,self._vocabulary.get_word(word_id)[1])

    @property
    def verbose(self):
        return self._verbose

    @verbose.setter
    def verbose(self, value):
        self._verbose = value

    @property
    def index_modalities(self):
        return self._index_modalities

    @index_modalities.setter
    def index_modalities(self, value):
        self._index_modalities = value

    def get_vocabulary(self):
        return self._vocabulary

    @property
    def vocabulary(self):
        return self._vocabulary

    def get_docword(self):
        return self._docword

    @property
    def docword(self):
        return self._docword

    def size(self):
        """
        Returns number of documents in collection
        """
        return len(self.docword)

    def get_document(self, doc_id, modality_list=None, doc_type='object'):
        """
        Returns content of documents by modalities in 'modality_list'. 
        Returned Value type can be (depends on doc_type argument)
            1) dictionary (doc_type='dict'), keys are modalities, values are list with document word_ids
            2) dictionary (doc_type='BOW'), keys are word_ids, values are counts of words
            3) list with word_ids of document (doc_type='list')
            4) unicode string (doc_type='plain_text'), string representation of document
            5) instance of Document class object (doc_type='object'). In this case modality_list
            argument will be ignored

        doc_id - ID of document
        modality_list - list of modalities which representations you need to obtain
        doc_type - returned value type
        """
        if modality_list is None:
            modality_list = self._vocabulary.modality_list()
        ret_val = None
        document = self.docword.get(doc_id, Document())
        if doc_type == 'object':
            ret_val= document
        elif doc_type == 'dict':
            ret_val = {}
            for mod in modality_list:
                ret_val[mod] = document.get(mod)
        elif doc_type == 'BOW':
            ret_val = document.get_bow(modality_list)
        elif doc_type == 'list':
            ret_val = []
            for mod in modality_list:
                for wid in document.get(mod):
                    ret_val += [self._vocabulary.get_word(wid)[0]]
        elif doc_type == 'plain_text':
            temp = []
            for mod in modality_list:
                for wid in document.get(mod):
                    temp += [self._vocabulary.get_word(wid)[0]]
            ret_val = u' '.join(temp)
        else:
            print(u"UNKNOWN doc_type in get_document() function")
            ret_val = document
        return ret_val

    def tf_df(self, modality):
        """
        Function counts term frequency and document frequency over collection for all enrtities of required 'modality' vocabulary
        Returned type value is dictionary. Keys are id of words, each value is list looks like [tf,df]
        """
        tf_df = {}
        if modality not in self._vocabulary.modality_list():
            return tf_df
        for word_id in self._vocabulary.modality_voc(modality):
            tf_df[word_id] = [0, 0]
        for doc in self.docword.values():
            doc_bow = doc.get_bow([modality])
            for word_id, count in doc_bow.iteritems():
                tf_df.setdefault(word_id, [0, 0])
                tf_df[word_id][0] += count
                tf_df[word_id][1] += 1
        return tf_df

    @staticmethod
    def _func_tfidf(tf, df, N):
        return float(tf)*np.log(float(N)/df)

    def save_collection_by_modalities(self, collection_name, folder=''):
        for mod in self.vocabulary.modality_list():
            filename = os.path.join(folder, u"{0}.{1}.txt".format(collection_name,mod))
            self.save_modality_in_file(mod,filename)
        for mod in self.index_modalities:
            filename = os.path.join(folder, u"index-{0}.{1}.txt".format(collection_name,mod))
            self.save_indexes_in_file(mod,filename)

    def count_tf_idf_by_classes(self, class_modality, word_modality, func_tfidf=None):
        if func_tfidf is None:
            func_tfidf = self._func_tfidf
        tf = {}
        df = {}
        tf_idf = {}
        for cl in self._vocabulary.modality_voc(class_modality):
            tf[cl] = {}
            tf_idf[cl] = {}
            for w_id in self._vocabulary.modality_voc(word_modality):
                tf[cl][w_id] = 0
#                tf_idf[cl][w_id] = 0

        for doc_id in self.docword:
            classes_list = self.get_document(doc_id,[class_modality],doc_type='BOW').keys()
            docBOW = self.get_document(doc_id,[word_modality],doc_type='BOW')
            for word_id, count in docBOW.items():
                df.setdefault(word_id,0)
                df[word_id] += 1
                for cl in classes_list:
                    tf[cl][word_id] += count

        for cl_id, cl_tf in tf.items():
            for w_id, count in cl_tf.items():
                tf_idf[cl_id][w_id] = func_tfidf(count, df[w_id], self.size())

        return tf_idf

    def tf_idf(self, modality, func=None):
        """
        Function counts tf-idf statistics over collection for all enrtities of required 'modality' vocabulary

        modality - modality we are interested in
        func - function for calculation tf-idf with 3 arguments, tf, df and lengh of collection (tf-idf formula).
            If func is None, the vanilla formula will be used: float(tf)*np.log(float(N)/df)

        Returned type value is dictionary. Keys are id of entities, values are tf-idf values
        """
        if func is None:
            func = self._func_tfidf
        tf_idf = {}
        N = len(self._docword)
        for key, val in self.tf_df(modality).iteritems():
            tf_idf[key] = func(val[0], val[1], N)
        return tf_idf

    def load_split_modality_files(self, collection_name, folder, index_files=True):
        """
        Function for loading collection from disk, building vocabulary.
            Function calls _find_content(...) function. Please find details of naming files in docsctring of that function

        collectionName - name of collection
        folder - folder to search files
        Return: vocabulary and collection objects
        """
        self._docword = self._content2collection(collection_name, folder, index_files)

    def shuffle(self):
        """
        Function for random shuffle of collection (shuffling of id documents).
        Note, that original order of documents will be lost
        """
        perm = np.random.permutation(self.size())
        keys = self._docword.keys()
        docword = {}
        for i in xrange(self.size()):
            doc_id = keys[perm[i]]
            docword[i+1] = self._docword[doc_id]
        self._docword = docword

    def saveAsUCI(self, collectionName, folder):
            """
            Function for storing collection and
                vocabulary (with modalities) in UCI format

            collectionName - name of collection.
            folder - folder for storing result files:
                vocab.collectionName.txt and docword.collectionName.txt
            """
            with codecs.open(folder+'/vocab.'+collectionName+'.txt', mode='w', encoding='utf-8') as f:
                for key in sorted(self._vocabulary._id2modality.keys()):
                    f.write(self._vocabulary.get_word(key)[0] + u' ' +
                            str(self._vocabulary._id2modality[key])+u'\n')
            NNW = 0
            for doc in self.docword.values():
                NNW += doc.length()
            with codecs.open(folder+'/docword.'+collectionName+'.txt', mode='w', encoding='utf-8') as f:
                f.write(str(self.size())+u'\n' +
                        str(self._vocabulary.size()) + u'\n' +
                        str(NNW) + u'\n')
                for doc_id in sorted(self._docword.keys()):
                    doc = self.get_document(doc_id, None, 'BOW')
                    for token_id, count in sorted(doc.items(),key=lambda x:x[0]):
                        f.write(u' '.join([str(doc_id),
                                          str(token_id),
                                          str(count)]) + u'\n')

    def split_train_test(self, train_fraction=0.8, trim_vocabulary=True):
        """
        Function for random splitting collection in 2 parts in defined fraction

        train_fraction - fraction for splitting collection. Default is 0.8
        trim_vocabulary - boolean value. If True then unused words will be removed from result collections. 
            Default value is True

        Function returns result as [trn_col, tst_col], where trn_col, tst_col are instances of class Collection
        """
        doc_ids = np.arange(1, len(self.docword)+1, 1)
        trn_col = Collection()
        tst_col = Collection()
        trn_col._index_modalities = self._index_modalities
        tst_col._index_modalities = self._index_modalities
        trn_doc_id = tst_doc_id = 1
        for doc_id in doc_ids:
            if np.random.rand() < train_fraction:
                trn_col._docword[trn_doc_id] = self._docword[doc_id]
                trn_doc_id += 1
            else:
                tst_col._docword[tst_doc_id] = self._docword[doc_id]
                tst_doc_id += 1
        trn_col._vocabulary = copy.deepcopy(self.vocabulary)
        tst_col._vocabulary = copy.deepcopy(self.vocabulary)
        if trim_vocabulary:
            to_remove_trn = []
            to_remove_tst = []
            for modality in self.get_vocabulary().modality_list():
                to_remove_trn.extend(trn_col.filter_by_freq(modality, 1, 1))
                to_remove_tst.extend(tst_col.filter_by_freq(modality, 1, 1))
            trn_col.remove_words(to_remove_trn)
            tst_col.remove_words(to_remove_tst)
        return trn_col, tst_col

    def split_by_number_per_category(self,
                                     cat_modality,
                                     num_per_cat):
        doc_ids = self.docword.keys()
        coll = Collection()
        coll._index_modalities = self.index_modalities
        coll_rest = Collection()
        coll_rest._index_modalities = self.index_modalities
        cat_frec = self.tf_df(cat_modality)
        doc_count = {}
        for cat in cat_frec:
            doc_count[cat] = 0
        perm = np.random.permutation(doc_ids)
        count = 1
        count_rest = 1
        for doc_id in perm:
            labels = self.get_document(doc_id).get(cat_modality)
            flag = False
            for label in labels:
                if doc_count[label] < num_per_cat:
                    doc_count[label] += 1
                    flag = True
            if flag:
                coll._docword[count] = self._docword[doc_id]
                count += 1
            else:
                coll_rest._docword[count_rest] = self._docword[doc_id]
                count_rest += 1
        coll._vocabulary = copy.deepcopy(self.vocabulary)
        coll_rest._vocabulary = copy.deepcopy(self.vocabulary)
        to_remove = []
        to_remove_rest = []
        for modality in self.get_vocabulary().modality_list():
            to_remove.extend(coll.filter_by_freq(modality, 1, 1))
            to_remove_rest.extend(coll_rest.filter_by_freq(modality, 1, 1))
        coll.remove_words(to_remove)
        coll_rest.remove_words(to_remove_rest)
        return coll, coll_rest

    def split_train_test_labeled(self, 
                                 label_modality, 
                                 train_fraction=0.8,
                                 trim_vocabulary=True):
        """
        Function for splitting collection on train and test parts.
        Returns 3 instances of Collection class stored as python dict:
            "unk" - documents without representation in 'label_modality' modality
            "trn" - balanced train part of labeled documents
                (fraction of train documents in all balanced documents is 'train_fraction')
            "tst" - rest of documents. If initially collection was not balanced by 'label_modality',
                then 'tst' collection will be unbalanced

        label_modality - name of modality with labels. Balancing dataset will be based on this modality.
        train_fraction - how many from balanced datacet should be in train dataset. Default is 0.8
        trim_vocabulary - boolean value. If True then unused words will be removed from result collections. 
            Default value is True

        NOTE! Function performs balancing train part: frequencies of all labels (from 'label_modality') will be equal.

        """
        if label_modality not in self.vocabulary.modality_list():
            return
        tf_df = self.tf_df(label_modality)
        prob = {}
        min_df = float(min(tf_df.values(), key=lambda x: x[1])[1])
        for label, [tf, df] in tf_df.iteritems():
            prob[label] = train_fraction * min_df/float(df)
        idx = {u'trn': [], u'tst': [], u'unk': []}
        for doc_id in self.docword:
            docs_labels = self.get_document(doc_id, [label_modality],doc_type='list')
            if not docs_labels:
                idx[u'unk'].append(doc_id)
                continue
            label = docs_labels[0]
            if np.random.rand() < prob[label]:
                idx[u'trn'].append(doc_id)
            else:
                idx[u'tst'].append(doc_id)
        collections = {u'trn': Collection(), u'tst': Collection(),
                       u'unk': Collection()}
        for v in collections.values():
            v._index_modalities = self.index_modalities
        for key, col in collections.iteritems():
            col._vocabulary = copy.deepcopy(self.vocabulary)
            for count, doc_id in enumerate(idx[key]):
                col._docword[count+1] = copy.copy(self._docword[doc_id])
            if trim_vocabulary:
                to_remove = []
                for modality in self.get_vocabulary().modality_list():
                    to_remove.extend(col.filter_by_freq(modality, 1, 1))
                col.remove_words(to_remove)
        return collections

    def remove_documents(self, doc_ids=[], trim_vocabulary=True):
        """
        Removes documents from collection.
        NOTE! Order of documetns will be lost!

        doc_ids - list of document IDs for removing
        trim_vocabulary - boolean value. If True then unused words will be removed from result collections. 
            Default value is True
        """
        for doc_id in doc_ids:
            self._docword.pop(doc_id)
        docword = {}
        for i, pair_id_doc in enumerate(sorted(self._docword.items(),key=lambda x:x[0])):
            docword[i+1] = pair_id_doc[1]
        self._docword = docword
        if trim_vocabulary:
            to_remove = []
            for modality in self.get_vocabulary().modality_list():
                to_remove.extend(self.filter_by_freq(modality, 1, 1))
            self.remove_words(to_remove)

    def remove_empty_documents(self,modality):
        """
        Removes empty documents which have empty representation in modality 'modality'
        NOTE! Order of documetns will be lost!
        """
        to_remove = []
        for doc_id in self._docword.keys():
            flag = True
            doc = self.get_document(doc_id,[modality],doc_type='list')
            if not doc:
                to_remove += [doc_id]
        self.remove_documents(to_remove)

    def remove_words(self, word_id_list=None, word_list=None, modality=None):
        """
        Function removes words by list of words id or by text representation of word and modality name
        """
        if word_list is None and word_id_list is None and modality is None:
            return
        if word_id_list is None:
            if word_list is None or modality is None or modality not in self._vocabulary.modality_list():
                print u'Set up modality and words'
            else:
                word_id_list = []
                for w in word_list:
                    if w in self._vocabulary._word2id[modality]:
                        word_id_list.append(
                            self._vocabulary.get_word_id(w, modality))
        if not word_id_list:
            return
        word_id_list = list(set(word_id_list))
        tmp = []
        for i, word_id in enumerate(word_id_list):
            if word_id < 1 or word_id > self._vocabulary.size():
                continue
            tmp.append(word_id)
        word_id_list = sorted(tmp)
        mapping = [0]*(self._vocabulary.size()+1)
        _id2modality = {}
        _voc = {}
        _word2id = {}
        shift = 0
        idx = 0
        for word_id in sorted(self._vocabulary._id2modality.keys()):
            if idx < len(word_id_list):
                if word_id == word_id_list[idx]:
                    idx += 1
                    shift -= 1
                    mapping[word_id] = 0
                    continue
            _word_id = word_id+shift
            mapping[word_id] = _word_id
            [word, mod] = self._vocabulary.get_word(word_id)
            _id2modality[_word_id] = mod
            _voc.setdefault(mod, {})
            _voc[mod][_word_id] = word
            _word2id.setdefault(mod, {})
            _word2id[mod][word] = _word_id

        self._vocabulary._id2modality = _id2modality
        self._vocabulary._word2id = _word2id
        self._vocabulary._voc = _voc
        for doc_id in self.docword.keys():
            doc = self.get_document(doc_id)
            d = Document()
            for word_id in doc:
                if mapping[word_id] == 0:
                    doc.remove_word(word_id)
            for mod, word_ids in doc.content.items():
                for i in xrange(len(word_ids)):
                    word_ids[i] = mapping[word_ids[i]]
                doc.content[mod] = word_ids
        # self._docword[doc_id] = doc

    def filter_by_freq(self, modality, min_tf=5, min_df=5, freq_filter=None):
        """
        Returns list of IDs of words with 'modality' for which function freq_filter returns True

        min_tf - treshhold of term frequency
        min_df - treshhold of document frequency
        freq_filter - functionwith 2 args: tf, df for filtering words
            Default value is 
                def freq_filter(tf, df): return (tf < min_tf) or (df < min_df)

        """
        if freq_filter is None:
            def freq_filter(tf, df): return (tf < min_tf) or (df < min_df)
        tf_df = self.tf_df(modality)
        result = []
        for word_id, [tf, df] in tf_df.iteritems():
            if freq_filter(tf, df):
                result.append(word_id)
        return result

    def save_modality_in_file(self, modality, filename, is_sorted=True):
        """
        Saving document representation in modality 'modality' to file
        Each line corresponds to one document.
        is_sorted - should sort lines by id in UCI collection 
        """
        with codecs.open(filename, mode='w', encoding='utf-8') as f:
            if is_sorted:
                for doc_id in sorted(self._docword.keys()):
                    f.write(self.get_document(
                        doc_id, modality_list=[modality], doc_type='plain_text') + u'\n')
            else:
                for doc_id in self._docword:
                    f.write(self.get_document_as_text(
                        doc_id, modality_list=[modality], doc_type='plain_text') + u'\n')

    def save_indexes_in_file(self, modality, filename, is_sorted=True):
        """
        Saving document indexes of modality 'modality' to file
        Each line corresponds to one document.
        is_sorted - should sort lines by id in UCI collection
        """
        with codecs.open(filename, mode='w', encoding='utf-8') as f:
            if is_sorted:
                for doc_id in sorted(self._docword.keys()):
                    doc = self.get_document(doc_id, modality_list=[modality])
                    indexes = []
                    [indexes.extend(pair) for pair in doc.get_indexes(modality)]
                    f.write(u" ".join([unicode(x) for x in indexes]) + u'\n')
            else:
                for doc_id in self._docword:
                    doc = self.get_document(doc_id, modality_list=[modality])
                    indexes = []
                    [indexes.extend(pair) for pair in doc.get_indexes(modality)]
                    f.write(u" ".join([unicode(x) for x in indexes]) + u'\n')

    def _find_content(self, collectionName, folder):
        """
        Find in 'folder' files collection 'collectionName'.
            (Assumed files are splitted by modalities)
        Files should be named as:
        collectionName.modalityName1.postfix1
        collectionName.modalityName2.postfix1
        collectionName.modalityName3.postfix1
        collectionName.modalityName1.postfix2
        collectionName.modalityName2.postfix2
        collectionName.modalityName3.postfix2
                        ...

        Files with same postfixes should be aligned by rows,
            e.g. lines with number #i in files
            'collectionName.modalityName1.postfix1',
            'collectionName.modalityName2.postfix1' and
            'collectionName.modalityName3.postfix1' related to the one document

        collectionName - name of collection
        folder - folder to search files
        """
        l = glob.glob(folder+'/'+collectionName+'.*')
        content = {}
        for name in l:
            basename = os.path.basename(name)
            spl = basename.split('.', 2)
            content.setdefault(spl[1], [])
            content[spl[1]].append(spl[2])
        if not self.verbose:
            print 'Found', len(content), 'modalities:', ', '.join(content.keys())
        return content

    def _find_index_files(self, collection_name, folder):
        """
        Function finds index files of collection in folder.
        Pattern of index file (corresponds to collection and modality):
            index-[collection_name].[modality_name].postfix

        collection_name - name of collection
        folder - folder to search files
        """
        files = glob.glob('{0}/index-{1}.*'.format(folder,collection_name))
        indexFiles = {}
        for name in files:
            basename = os.path.basename(name)
            spl = basename.split('.', 2)
            indexFiles.setdefault(spl[1], [])
            indexFiles[spl[1]].append(spl[2])
        return indexFiles

    def _content2collection(self, collectionName, folder, index_files=True):
        """
        Function for loading collection from disk, building vocabulary.
            Function calls collectionContent(...) function.

        collectionName - name of collection
        folder - folder to search files
        index_files - enabling/disabling search of files with indexes
        Return: vocabulary and collection objects
        """
        collection = {}
        contentFiles = self._find_content(collectionName, folder)
        indexFiles = {}
        if index_files:
            indexFiles = self._find_index_files(collectionName, folder)
        self._index_modalities = [mod for mod in indexFiles.keys() if contentFiles.has_key(mod)]
        parts = {}
        for key in contentFiles.keys():
            for part in contentFiles[key]:
                parts.setdefault(part, [])
                parts[part].append(key)

        index_parts = {}
        for key in indexFiles.keys():
            for part in indexFiles[key]:
                index_parts.setdefault(part, [])
                index_parts[part].append(key)


        doc_id = 1
        for i in parts.keys():
            files = {}
            lines = {}
            index_files = {}
            index_lines = {}
            for modality in parts[i]:
                self._vocabulary._voc.setdefault(modality, {})
                self._vocabulary._word2id.setdefault(modality, {})

                filename = "{0}/{1}.{2}.{3}".format(folder, collectionName, modality, i)
                files[modality] = open(filename, 'r')
                lines[modality] = u' '

            for modality in index_parts.get(i,[]):
                filename = "{0}/index-{1}.{2}.{3}".format(folder, collectionName, modality, i)
                index_files[modality] = open(filename, 'r')

            while lines.values()[0]:
                for key in files.keys():
                    lines[key] = files[key].readline().decode('utf-8')
                for key in index_files.keys():
                    index_lines[key] = index_files[key].readline().decode('utf-8')

                doc = Document()
                for key in lines.keys():
                    if not len(lines[key]):
                        continue
                    ids = []
                    index_line = index_lines.get(key,u"").strip(u'\r\n\0')
                    if not len(index_line):
                        ids = []
                    else:
                        ids = [int(x) for x in index_line.split(u" ")]
                    indexes = zip(ids[0::2], ids[1::2])
                    token_num = 0
                    for token in lines[key].split(u' '):
                        token = token.strip(u'\r\n')
                        if not len(token):
                            continue
                        length = len(self.vocabulary._id2modality) + 1
                        tok_id = self._vocabulary._word2id[key].setdefault(token, length)
                        if tok_id == length:
                            self._vocabulary._id2modality[tok_id] = key
                            self._vocabulary._voc[key][tok_id] = token
                        if indexes:
                            doc.add_word_and_index(tok_id,key,indexes[token_num])
                        else:
                            doc.add_word(tok_id, key)
                        token_num += 1
                if doc.length():
                    collection[doc_id] = doc
                    doc_id += 1

            for key in files.keys():
                files[key].close()
        return collection

    def save_as_vw(self, folder='', filename=None, vocab_filename=None, filename_modality=None, bow=True):
        if filename is None:
            filename = 'data.vw.txt'
        if filename_modality is None:
            mods = self._vocabulary.modality_list()
        else:
            mods = self._vocabulary.modality_list()
            mods.remove(filename_modality)

        with codecs.open(folder+filename, mode='w', encoding='utf-8') as f:
            for index in sorted(self.docword.keys()):
                newline = u""
                if filename_modality is None:
                    newline += unicode(index)+u" "
                else:
                    newline += self.get_document(index,[filename_modality],'plain_text') + u" "
                if bow:
                    for mod in mods:
                        docBOW = self.get_document(index, [mod], 'BOW')
                        if not docBOW:
                            continue
                        newline += u'|{0} '.format(mod)
                        for k,v in docBOW.iteritems():
                            newline += u'{0}:{1} '.format(self._vocabulary.get_word(k)[0],v)
                else:
                    for mod in mods:
                        doc = self.get_document(index, [mod], 'plain_text')
                        if not doc:
                            continue
                        newline += u'|{0} {1} '.format(mod, doc)
                f.write(newline+u"\n")

        if not vocab_filename is None:
            with codecs.open(folder+vocab_filename, mode='w', encoding='utf-8') as f:
                for key in sorted(self._vocabulary._id2modality.keys()):
                    if unicode(self._vocabulary._id2modality[key]) == unicode(filename_modality):
                        continue
                    f.write(self._vocabulary.get_word(key)[0] + u' ' +
                            unicode(self._vocabulary._id2modality[key])+u'\n')

    def make_collection_with_words(self, word_id_list):
        coll = Collection()
        coll._vocabulary = copy.deepcopy(self._vocabulary)
        coll._index_modalities = copy.deepcopy(self.index_modalities)
        id = 1
        for doc_id, doc in self.docword.iteritems():
            docBOW = doc.get_bow()
            flag = True
            for w in word_id_list:
                if not docBOW.has_key(w):
                    flag = False
                    break
            if flag:
                coll._docword[id] = copy.deepcopy(doc)
                id += 1
        to_remove = []
        for modality in coll.vocabulary.modality_list():
            to_remove += coll.filter_by_freq(modality, 1, 1)
        coll.remove_words(to_remove)
        return coll

    def ppmi_of_file_by_vocabulary(self,
                                   in_filename,
                                   out_filename,
                                   modality,
                                   ppmi_func=None,
                                   window_size=10,
                                   word_id=True):
        """
        Function for counting pPMI statistics by text corpora based on vocabulary of collection
        It is assumed that tokens are devided by space symbol

        in_filename - file with text corpora for statistic counting
        out_filename - file for writing ppmi statistic
        modality - name of modality which correspond text corpora
        ppmi_func - function for statistic calculation
            function should have 4 args
            if ppmi_func is None, so vanilla ppmi function will be used
        window_size - size of window for statistic calculation
        word_id - boolean flag. Default is True
            if True in output file will be stored IDs of words (if indexing starts in 0!!! BigARTM requirement!!!)
            else in output file will be stored text representation of words
        """
        if ppmi_func is None:
            ppmi_func = lambda xy, x, y, dataset_length: max(
                                      [0, np.log(xy*dataset_length/x/y)]
                                                          )
        words = []
        with codecs.open(in_filename,'r','utf-8') as f:
            words = [line.strip(u'\r\n\0').split(u" ") for line in f]
        voc = self.get_vocabulary()
        pair_count = {}
        word_count = {}
        dataset_length = float(sum([len(x) for x in words]))
        for line in words:
            for start in xrange(len(line)):
                start_voc_idx = voc.get_word_id(line[start],modality)
                if start_voc_idx == -1:
                    continue
                word_count.setdefault(start_voc_idx,0)
                word_count[start_voc_idx] += window_size
                end = min([start + window_size,len(line)])
                for idx in xrange(start+1,end):
                    voc_idx = voc.get_word_id(line[idx],modality)
                    if voc_idx == -1:
                        continue
                    tuple_idxs = tuple(sorted([voc_idx,start_voc_idx]))
                    pair_count.setdefault(tuple_idxs,0)
                    pair_count[tuple_idxs] += window_size - idx + start
        with codecs.open(out_filename,'w','utf-8') as f:
            for k, v in pair_count.iteritems():
                s1 = u"{0} {1} "
                if word_id:
                    s1 = s1.format(k[0]-1,k[1]-1)
                else:
                    s1 = s1.format(voc.get_word(k[0])[0],
                                   voc.get_word(k[1])[0])
                s2 = u"{0:.3f}\n"
                s2 = s2.format(ppmi_func(float(v), word_count[k[0]],
                                         word_count[k[1]], dataset_length))
                f.write(s1+s2)


if __name__ == '__main__':
    collectionName = 'lenta'
    inFolder = '/home/aromanenko/workspace/data/lenta/UCI/'
    outFolder = {'tst': '/home/aromanenko/workspace/data/lenta/tst/',
                 'trn': '/home/aromanenko/workspace/data/lenta/trn/',
                 'trn_tst': '/home/aromanenko/workspace/data/lenta/trn_tst/',
                 'unk': '/home/aromanenko/workspace/data/lenta/unk/'}
    col = Collection(inFolder + 'vocab.lenta.txt',
                     inFolder +'docword.lenta.txt')
    collections = col.split_train_test_labeled('category')
#    c = copy.deepcopy(col)
#    cat_labels = c.get_vocabulary().modality_voc('category').values()
#    c.remove_words(None, cat_labels, 'category')
#    print len(c.tf_df('category'))
    for i, [tf, df] in collections['trn'].tf_df('category').iteritems():
        print collections['trn'].get_vocabulary().get_word(i)[0], df

    collections['tst'].save_modality_in_file('category',
        '/home/aromanenko/workspace/data/lenta/labels/tst.category.txt')
    collections['trn'].save_modality_in_file('category',
        '/home/aromanenko/workspace/data/lenta/labels/trn.category.txt')
    collections['trn'].saveAsUCI('lenta', outFolder['trn'])
    cat_labels = col.get_vocabulary().modality_voc('category').values()
    collections['trn'].remove_words(word_id_list=None,
        word_list=cat_labels,modality='category')
    collections['tst'].remove_words(word_id_list=None,
        word_list=cat_labels,modality='category')
    collections['trn'].saveAsUCI('lenta', outFolder['trn_tst'])
    collections['tst'].saveAsUCI('lenta', outFolder['tst'])


