# -*- coding: utf-8 -*-
"""
Created on Tue Jul 5 16:14:04 2016

@author: aleksandr.romanenko@phystech.edu
"""

import re
import json
import numpy as np
import os
import struct

class ModelARTM(object):
    '''Class for maintenance models with modalities. Class contains 
    vocabularies and matrices Phi for modalities'''
   
    def __init__(self, folder, alpha=0.05,int_size=4, float_size=4):
        self.initialize(folder, alpha, int_size, float_size)
    
    def _loadModalities(self,filename):    
        modalities = {}
        with open(filename,'r') as f:
            for line in f:
                line = line.strip('\r\n\0').decode('utf-8')
                splt = line.split(u' ')
                modalities[splt[0]] = float(splt[1])
        return modalities

    def _loadVocabulary(self,filename):
        v = {}
        with open(filename,'r') as f:
            i = 0
            for line in f:
                v[line.strip('\r\n\0').decode('utf-8')] = i
                i += 1
        return v
        
    def _loadSparseMatrix(self,filename,int_size,float_size):
        mul = 65535.0
        int_type = 'i'
        float_type = 'd'
        if int_size == 2:
            int_type = 'H'
        if float_size == 4:
            float_type = 'f'
        if float_size == 2:
            float_type = 'H'
        with open(filename,'rb') as f:
            rows,cols = 0,0
            rows = struct.unpack(int_type,f.read(int_size)[::-1])[0]
            cols = struct.unpack(int_type,f.read(int_size)[::-1])[0]
            mat = dict([(r,{}) for r in range(rows)])
            byte_arr = f.read(int_size)[::-1]
            while (byte_arr):
                row = struct.unpack(int_type,byte_arr)[0]
                byte_arr = f.read(int_size)[::-1]
                count = struct.unpack(int_type,byte_arr)[0]
                for j in range(count):
                    byte_arr = f.read(int_size)[::-1]
                    col = struct.unpack(int_type,byte_arr)[0]
                    byte_arr = f.read(float_size)[::-1]
                    val = struct.unpack(float_type,byte_arr)[0]
                    if float_size == 2:
                        val = float(val)/mul
                        mat[row][col] = val
                    else:
                        mat[row][col] = val
                byte_arr = f.read(int_size)[::-1]
        return mat, rows, cols
    
    def initialize(self, folder, alpha=0.05, int_size=4, float_size=4):
        self._alpha = alpha
        self._modalities = self._loadModalities(
                            os.path.join(folder,'modalities.txt'))
        self._Vocabulary = {}
        self._Pwt = {}
        for mod in self._modalities:
            filename = os.path.join(folder,mod+'_vocabulary.txt')
            self._Vocabulary[mod] = self._loadVocabulary(filename)
            filename = os.path.join(folder,mod+'_phi.bin')
            self._Pwt[mod], rows, cols = self._loadSparseMatrix(filename,
                                                                int_size,
                                                                float_size)
            self._topicNumber = cols
            if (rows != len(self.Vocabulary[mod])):
                to_print = "Matrix size ({}) and ".format(rows)
                to_print += "vocabulary length ({}) ".format(
                                                len(self.Vocabulary[mod]))
                to_print += "are not equal ({} modality)".format(mod)
                raise Exception(to_print)        
                
    @property
    def Pwt(self):
        return self._Pwt

    @property
    def Vocabulary(self):
        return self._Vocabulary

    @property
    def modalities(self):
        return self._modalities

    @property
    def alpha(self):
        return self._alpha
        
    @alpha.setter
    def alpha(self,alpha):
        self._alpha = alpha

    @property
    def topicNumber(self):
        return self._topicNumber
        
    def __getitem__(self,arg):
        mod,w,j = arg
        if isinstance(w,int):
            i = w
        else:
            i = self.Vocabulary[mod].get(w,-1)
        res = 0.0
        if self._Pwt[mod].has_key(i):
            res = self._Pwt[mod][i].get(j,0.0)
        return res
    
    def wordId(self,modality,word):
        return self.Vocabulary.get(modality,{}).get(word,-1)
    
    def getWordByID(self,modality,idx):
        for word,i in self.Vocabulary[modality].iteritems():
            if i == idx:
                return word
        return u''
        

class InferenceARTM(object):
    '''Class for performing Inference like in BigARTM library'''
    def __init__(self, model, iterationForDocument=5):
        self._model = model
        self._iterationForDocument = iterationForDocument
    
    @property
    def iterationForDocument(self):
        return self._iterationForDocument

    @iterationForDocument.setter
    def iterationForDocument(self,iterationForDocument):
        self._iterationForDocument = iterationForDocument
    
    def find_theta(self, doc, modality, BOW=False):
        if BOW:
            docBOW = doc
        else:
            docBOW = dict()
            for w in doc:
                docBOW[w] = docBOW.get(w, 0.0) + 1.0
        topicNumber = self._model.topicNumber        
        theta = np.ones(topicNumber,dtype=float) / topicNumber
        for i in xrange(self.iterationForDocument):
            ntd = np.zeros(topicNumber,dtype=np.float128)

            # use field.token_count for old batches (if token_weight is empty)
            for word,count in docBOW.iteritems():
                pWt = np.array([self._model[modality,word,j] for j in range(topicNumber)],dtype=np.float128)
                pwt = theta*pWt
                zw = sum(pwt)
                if not zw:
                    continue
                
                a = float(count) / zw
                ntd += a * pWt
            theta *= ntd
            theta += self._model.alpha
            theta = np.maximum(theta, 0.0)
            theta /= sum(theta)
        return theta
        
    def classify(self, target_modality, theta=None, doc=None, data_modality=None, BOW=False):
        if doc is None and theta is None:
            return []
        if doc is not None:
            theta = self.find_theta(doc,data_modality,BOW)
        tN = self._model.topicNumber
        prob = np.zeros(len(self._model.Vocabulary[target_modality]),float)
        for w,idx in self._model.Vocabulary[target_modality].iteritems():
            prob[idx] += sum([theta[j] * self._model[target_modality,w,j] for j in range(tN)])
        return prob
        

if __name__ == "__main__":
    
    def loadFileAsStringArray(filename, empty_strings=False):
        content = []
        f = open(filename,'r')
        for line in f:
            line = line.strip('\r\n\0')
            if not empty_strings and len(line) == 0:
                continue
            content.append(line)
        f.close()
        return content
    
    filename = '../postnauka/ngramm/pymorphy/pn.ngramm.txt'
#    filename = "/home/aromanenko/workspace/Java/BigartmInferenceShort/test.txt"
    corpora = []
    for l in loadFileAsStringArray(filename):
        corpora += [l.decode('utf-8').split(' ')]    
    
    model = ModelARTM('../postnauka/model_44/',0.05,4,4)
#    model = ModelARTM("/home/aromanenko/workspace/Java/models/model_22/",0.05,2,2)
#    mod =u'category'
    mod =u'author'
    tags = [z[0] for z in sorted(model.Vocabulary[mod].items(),key=lambda x:x[1])]
    inferencer = InferenceARTM(model,5)
    for doc in corpora[:100]:
#        doc = corpora[0]
        theta = inferencer.find_theta(doc,u'ngramm')
        pcd = inferencer.classify(mod,theta)
        for ix in np.argsort(pcd)[::-1]:
            if pcd[int(ix)] < 0.02:
                break
            print tags[int(ix)].encode('utf-8'),
#            print tags[int(ix)].encode('utf-8'),pcd[int(ix)], 
        print ''
#    for w in doc:
#        print w
#        for x in range(model.topicNumber):
#            if model['ru',w,x]:
#                print model['ru',doc[0],x],
